package com.eviv.jrTest.controller;

import com.eviv.jrTest.model.User;
import com.eviv.jrTest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
public class UserController {
    private UserService userService;

    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public String listUsers(@RequestParam(value = "page", required = false) Integer page,
                            Model model) {
        model.addAttribute("user", new User());
        if (page == null)
            page = 0;
        int countUsers = this.userService.countUsers();
        int pagesTotal = (int) Math.ceil(countUsers / (float)this.userService.getPageLimit());
        model.addAttribute("listUsers", this.userService.listUsers(page));
        model.addAttribute("pageNumber", page.intValue());
        model.addAttribute("count", countUsers);
        model.addAttribute("pagesTotal", pagesTotal);
        model.addAttribute("pageLimit", this.userService.getPageLimit());

        return "users";
    }

    @RequestMapping(value = "users/search", method = RequestMethod.GET)
    public String findUsers(@RequestParam(value = "page", required = false) Integer page,
                            @RequestParam(value = "name", required = true) String userName,
                            Model model) {
        model.addAttribute("user", new User());
        if (page == null)
            page = 0;
        int countUsers = this.userService.countUsers(userName);
        int pagesTotal = (int) Math.ceil(countUsers / (float)this.userService.getPageLimit());
        model.addAttribute("listUsers", this.userService.listUsers(page, userName));
        model.addAttribute("pageNumber", page.intValue());
        model.addAttribute("count", countUsers);
        model.addAttribute("pagesTotal", pagesTotal);
        model.addAttribute("backToListLink", true);
        model.addAttribute("pageLimit", this.userService.getPageLimit());

        return "users";
    }

    @RequestMapping(value = "users/page-limit", method = RequestMethod.GET)
    public String changePageLimit(@RequestParam(value = "pageLimit", required = true) Integer limit) {
        if (limit == null) {
            limit = 3;
        }
        this.userService.setPageLimit(limit);

        return "redirect:/users";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user) {
        if (user.getId() == 0) {
            user.setCreatedDate(new Date());
            this.userService.addUser(user);
        } else {
            this.userService.updateUser(user);
        }

        return "redirect:/users";
    }

    @RequestMapping("add-user")
    public String addUser(Model model) {
        model.addAttribute("user", new User());
        return "add-user";
    }

    @RequestMapping("/remove/{id}")
    public String removeUser(@PathVariable("id") int id) {
        this.userService.removeUser(id);
        return "redirect:/users";
    }

    @RequestMapping("edit/{id}")
    public String editUser(@RequestParam(value = "page", required = false) Integer page, @
            PathVariable("id") int id, Model model) {
        model.addAttribute("user", this.userService.getUserById(id));
        if (page == null)
            page = 0;
        model.addAttribute("listUsers", this.userService.listUsers(page));
        model.addAttribute("count", this.userService.countUsers());
        model.addAttribute("pageNumber", page);
        return "add-user";
    }

}
