package com.eviv.jrTest.dao;

import com.eviv.jrTest.model.User;

import java.util.List;

/**
 * Created by j3v on 14.08.16.
 */
public interface UserDao {
    public void setPageLimit(int pageLimit);
    public int getPageLimit();
    public void addUser(User user);
    public void updateUser(User user);
    public void removeUser(int id);
    public User getUserById(int id);
    public List<User> listUsers(int page);
    public int countUsers();
    public List<User> listUsers(int page, String userName);
    public int countUsers(String userName);

}
