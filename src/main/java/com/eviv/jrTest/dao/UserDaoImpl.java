package com.eviv.jrTest.dao;

import com.eviv.jrTest.model.User;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    public static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
    public static int pageLimit = 10;

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void setPageLimit(int pageLimit) {
        UserDaoImpl.pageLimit = pageLimit;
    }

    @Override
    public int getPageLimit() {
        return pageLimit;
    }

    @Override
    public void addUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
        logger.info("User successfully saved. User details: " + user);
    }

    @Override
    public void updateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
        logger.info("User successfully updated. User details: " + user);
    }

    @Override
    public void removeUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, new Integer(id));

        if (user != null) {
            session.delete(user);
        }
        logger.info("User successfully deleted. User details: " + user);
    }

    @Override
    public User getUserById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, new Integer(id));
        logger.info("User successfully loaded. User details: " + user);

        return user;
    }

    @Override
    public List<User> listUsers(int page) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User");
        query.setFirstResult(page * pageLimit);
        query.setMaxResults(pageLimit);
        List<User> userList = query.list();
        return userList;
    }

    @Override
    public int countUsers() {
        Session session = this.sessionFactory.getCurrentSession();
        Integer result = ((Number) (session.createQuery("select count(*) from User").uniqueResult())).intValue();
        return result;
    }

    @Override
    public List<User> listUsers(int page, String userName) {
        Session session = this.sessionFactory.getCurrentSession();
        String queryString = "from User u where u.name = '" + userName + "'";
        Query query = session.createQuery(queryString);
        query.setFirstResult(page * pageLimit);
        query.setMaxResults(pageLimit);
        List<User> userList = query.list();
        return userList;
    }

    @Override
    public int countUsers(String userName) {
        Session session = this.sessionFactory.getCurrentSession();
        String queryString = "select count(*) from User u where u.name = '" + userName + "'";
        Integer result = ((Number) (session.createQuery(queryString).uniqueResult())).intValue();
        return result;
    }
}
