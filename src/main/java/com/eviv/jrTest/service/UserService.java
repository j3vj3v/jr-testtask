package com.eviv.jrTest.service;

import com.eviv.jrTest.model.User;

import java.util.List;

public interface UserService {
    public void setPageLimit(int pageLimit);
    public int getPageLimit();
    public void addUser(User user);
    public void updateUser(User user);
    public void removeUser(int id);
    public User getUserById(int id);
    public List<User> listUsers(int page);
    public List<User> listUsers(int page, String userName);
    public int countUsers();
    public int countUsers(String userName);

}
