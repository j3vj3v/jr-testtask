package com.eviv.jrTest.service;

import com.eviv.jrTest.dao.UserDao;
import com.eviv.jrTest.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void setPageLimit(int pageLimit) {
        this.userDao.setPageLimit(pageLimit);
    }

    @Override
    public int getPageLimit() {
        return this.userDao.getPageLimit();
    }

    @Override
    @Transactional
    public void addUser(User user) {
        this.userDao.addUser(user);
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        this.userDao.updateUser(user);
    }

    @Override
    @Transactional
    public void removeUser(int id) {
        this.userDao.removeUser(id);
    }

    @Override
    @Transactional
    public User getUserById(int id) {
        return this.userDao.getUserById(id);
    }

    @Override
    @Transactional
    public List<User> listUsers(int page, String userName) {
        return this.userDao.listUsers(page, userName);
    }

    @Override
    @Transactional
    public int countUsers() {
        return this.userDao.countUsers();
    }

    @Override
    @Transactional
    public int countUsers(String userName) {
        return this.userDao.countUsers(userName);
    }

    @Override
    @Transactional
    public List<User> listUsers(int page) {
        return this.userDao.listUsers(page);
    }
}
