<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users Page</title>
</head>
    <a href="../../index.jsp">Back to main menu</a>

    <br/>
    <br/>

    <h1>Users List</h1>

    <c:if test="${!empty listUsers}">

        <c:url value="users/page-limit" var="pageLimitUrl"/>
        <form:form action="${pageLimitUrl}" method="get">
            Lines per page:
            <input type="text" name="pageLimit" value="${pageLimit}"/>
            <input type="submit" value="Set"/>
        </form:form>

        <c:url value="users/search" var="search"/>
        <form:form action="${search}" method="get">
            User name to search:
            <input type="text" name="name"/>
            <input type="submit" value="Search"/>
        </form:form>
        <div>Page: ${pageNumber + 1} of ${pagesTotal}</div>
        <c:choose>
            <c:when test="${pageNumber gt 0}">
                <c:url value="/users" var="firstPageUrl">
                    <c:param name="page" value="0"/>
                </c:url>
                <a href="${firstPageUrl}">&Lt;</a>
                <c:url value="/users" var="prevPageUrl">
                    <c:param name="page" value="${pageNumber - 1}"/>
                </c:url>
                <a href="${prevPageUrl}">&lt;</a>
            </c:when>
            <c:otherwise>
                &Lt; &lt;
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${pageNumber < (pagesTotal - 1)}">
                <c:url value="/users" var="nextPageUrl">
                    <c:param name="page" value="${pageNumber + 1}"/>
                </c:url>
                <a href="${nextPageUrl}">&gt;</a>
                <c:url value="/users" var="lastPageUrl">
                    <c:param name="page" value="${pagesTotal - 1}"/>
                </c:url>
                <a href="${lastPageUrl}">&Gt;</a>
            </c:when>
            <c:otherwise>
                &gt; &Gt;
            </c:otherwise>
        </c:choose>
        <table border="1">
            <tr>
                <th width="80">ID</th>
                <th width="120">Name</th>
                <th width="120">Age</th>
                <th width="80">Is Admin</th>
                <th width="120">Date</th>
                <th width="60">Edit</th>
                <th width="60">Delete</th>
            </tr>
            <c:forEach items="${listUsers}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.name}</td>
                    <td>${user.age}</td>
                    <td>${user.admin}</td>
                    <td>${user.createdDate}</td>
                    <td><a href="<c:url value="/edit/${user.id}"/>">Edit</a></td>
                    <td><a href="<c:url value="/remove/${user.id}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
        <div>Total amount of users: ${count}</div>
    </c:if>
    <br/>
    <a href="<c:url value="/add-user"/>">Add new user</a>
    <br/>
    <c:if test="${!empty backToListLink}">
        <a href="<c:url value="/users"/>">Back to users list</a>
    </c:if>
</body>
</html>
